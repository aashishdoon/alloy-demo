﻿using System;
using NUnit.Framework;
using Pricing.Demo.Models;
using Pricing.Demo.Services;
using Pricing.Demo.Services.Contracts;

namespace Pricing.Demo.Tests
{
    [TestFixture]
    public class PricingServiceTests
    {
        private IPricingService _pricingService;
        private User _user;

        [SetUp]
        public void Init()
        {
            _pricingService = new PricingService();
            _user = new User
            {
                IsLoggedIn = true
            };
        }

        /// <summary>
        /// ************ Scenario 01 ************
        /// User                    :: Logged in
        /// Organizational discount	:: 7%
        /// Catalog price	        :: 36.00
        /// Online discount         :: Yes
        /// Max discount            :: NULL
        /// </summary>
        [Test]
        public void Test_Scenario_01()
        {
            var product = new Product
            {
                CataloguePrice = 36,
                OnlineDiscount = true
            };

            _user.OrgDiscount = 7;

            const decimal expectedPrice = 32.94m;

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ShowPrice);
                Assert.That(result.Price, Is.EqualTo(expectedPrice));
            });
        }

        /// <summary>
        /// ************ Scenario 02 ************
        /// User                    :: Logged in
        /// Organizational discount	:: 12%
        /// Catalog price	        :: 128.99
        /// Online discount         :: No
        /// Max discount            :: NULL
        /// </summary>
        [Test]
        public void Test_Scenario_02()
        {
            var product = new Product
            {
                CataloguePrice = 128.99m,
                OnlineDiscount = false
            };

            _user.OrgDiscount = 12;

            const decimal expectedPrice = 113.5112m;

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ShowPrice);
                Assert.That(result.Price, Is.EqualTo(expectedPrice));
            });
        }

        /// <summary>
        /// ************ Scenario 03 ************
        /// User                    :: Anonymous
        /// Organizational discount	:: NULL
        /// Catalog price	        :: 128.99
        /// Online discount         :: Yes
        /// Max discount            :: NULL
        /// </summary>
        [Test]
        public void Test_Scenario_03()
        {
            _user.IsLoggedIn = false;

            var product = new Product
            {
                CataloguePrice = (decimal)128.99,
                OnlineDiscount = true
            };

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsFalse(result.ShowPrice);
            });
        }

        /// <summary>
        /// ************ Scenario 04 ************
        /// User                    :: Logged in
        /// Organizational discount	:: 15%
        /// Catalog price	        :: 36.00
        /// Online discount         :: No
        /// Max discount            :: 10%
        /// </summary>
        [Test]
        public void Test_Scenario_04()
        {
            var product = new Product
            {
                CataloguePrice = 36,
                OnlineDiscount = false,
                MaxDiscount = 10
            };

            _user.OrgDiscount = 15;

            const decimal expectedPrice = 32.4m;

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ShowPrice);
                Assert.That(result.Price, Is.EqualTo(expectedPrice));
            });
        }

        /// <summary>
        /// ************ Scenario 05 ************
        /// User                    :: Logged in
        /// Organizational discount	:: 15%
        /// Catalog price	        :: 36.00
        /// Online discount         :: No
        /// Max discount            :: 14%
        /// </summary>
        [Test]
        public void Test_Scenario_05()
        {
            var product = new Product
            {
                CataloguePrice = 36,
                OnlineDiscount = false,
                MaxDiscount = 14
            };

            _user.OrgDiscount = 15;

            const decimal expectedPrice = 30.96m;

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ShowPrice);
                Assert.That(result.Price, Is.EqualTo(expectedPrice));
            });
        }

        /// <summary>
        /// ************ Scenario 06 ************
        /// User                    :: Logged in
        /// Organizational discount	:: 15%
        /// Catalog price	        :: 36.00
        /// Online discount         :: Yes
        /// Max discount            :: 14%
        /// </summary>
        [Test]
        public void Test_Scenario_06()
        {
            var product = new Product
            {
                CataloguePrice = 36,
                OnlineDiscount = true,
                MaxDiscount = 14
            };

            _user.OrgDiscount = 15;

            const decimal expectedPrice = 30.96m;

            var result = _pricingService.GetPrice(product, _user);

            Console.WriteLine(result.Message);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ShowPrice);
                Assert.That(result.Price, Is.EqualTo(expectedPrice));
            });
        }
    }
}
