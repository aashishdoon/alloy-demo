﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Alloy.Demo.Models.Pages
{
    [ContentType(
        GUID = "44a87fd0-f109-476a-a36a-b53280c35fbe",
        GroupName = Global.GroupNames.Specialized)]
    [AvailableContentTypes(
        Availability.Specific,
        Include = new[] { typeof(EventPage) })]
    [SiteImageUrl]

    public class EventListingPage : SitePageData
    {
        
    }
}