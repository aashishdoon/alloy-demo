﻿using System;
using System.ComponentModel.DataAnnotations;
using Alloy.Demo.Business.EditorDescriptors;
using Alloy.Demo.Business.SelectionFactories;
using Alloy.Demo.Models.Enums;
using EPiServer.Web;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;

namespace Alloy.Demo.Models.Pages
{
    [ContentType(
        GUID = "b74119ad-962c-43f8-8601-8f76ebb9820c",
        GroupName = Global.GroupNames.Specialized)]
    [AvailableContentTypes(
        Availability.Specific,
        Include = new[] { typeof(StandardPage), typeof(ContainerPage) },
        IncludeOn = new[] { typeof(EventListingPage) })]
    [SiteImageUrl]
    public class EventPage : StandardPage
    {
        [UIHint(UIHint.Textarea)]
        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 10)]
        public virtual string Summary { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 20)]
        public virtual XhtmlString Description { get; set; }

        [Required]
        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 10)]
        public virtual string Title { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 20)]
        [SelectOne(SelectionFactoryType = typeof(EventSpeakerSelectionFactory))]
        public virtual string Speaker { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 30)]
        [Range(1, 100, ErrorMessage = "NoAttendees must be between 1 and 100")]
        public virtual int NoAttendees { get; set; }

        [Required]
        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 40)]
        public virtual DateTime StartDate { get; set; }

        [Required]
        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 50)]
        public virtual DateTime EndDate { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 60)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference EventImage { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 70)]
        public virtual ContentArea AdditionalContent { get; set; }

        [Display(
            GroupName = Global.GroupNames.EventDetail,
            Order = 80)]
        [CultureSpecific]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<EventStatus>))]
        public virtual EventStatus EventStatus { get; set; }
    }
}