﻿using System.Collections.Generic;
using Alloy.Demo.Models.Pages;

namespace Alloy.Demo.Models.ViewModels
{
    public class EventListingViewModel : PageViewModel<EventListingPage>
    {
        public IEnumerable<EventPage> AllEvents { get; set; }
        public EventListingViewModel(EventListingPage currentPage) : base(currentPage)
        {
        }
    }
}