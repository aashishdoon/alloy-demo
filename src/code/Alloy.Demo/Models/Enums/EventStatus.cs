﻿namespace Alloy.Demo.Models.Enums
{
    public enum EventStatus
    {
        Unknown,
        Closed
    }
}