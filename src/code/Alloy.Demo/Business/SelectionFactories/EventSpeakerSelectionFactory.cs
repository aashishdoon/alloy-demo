﻿using System.Linq;
using System.Collections.Generic;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;

namespace Alloy.Demo.Business.SelectionFactories
{
    public class EventSpeakerSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var categoryRepository = ServiceLocator.Current.GetInstance<CategoryRepository>();
            var caregoryRoot = categoryRepository.Get(Global.Categories.EventSpeakers);

            var filterCategory = caregoryRoot.Categories.ToList();

            var selectItems = new List<ISelectItem>();

            foreach (var category in filterCategory)
            {
                selectItems.Add(new SelectItem
                {
                    Text = category.LocalizedDescription,
                    Value = category.ID
                });
            }
            return selectItems;
        }
    }
}