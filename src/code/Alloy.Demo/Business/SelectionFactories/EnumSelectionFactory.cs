﻿using System;
using System.Collections.Generic;
using EPiServer.Framework.Localization;
using EPiServer.Shell.ObjectEditing;

namespace Alloy.Demo.Business.SelectionFactories
{
    public class EnumSelectionFactory<TEnum> : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(
            ExtendedMetadata metadata)
        {
            var values = Enum.GetValues(typeof(TEnum));
            foreach (var value in values)
            {
                yield return new SelectItem
                {
                    Text = GetValueName(value),
                    Value = value
                };
            }
        }

        private string GetValueName(object value)
        {
            var staticName = Enum.GetName(typeof(TEnum), value);

            var localizationPath =
                $"/property/enum/{typeof(TEnum).Name.ToLowerInvariant()}/{staticName?.ToLowerInvariant()}";

            return LocalizationService.Current.TryGetString(
                localizationPath,
                out string localizedName) ? localizedName : staticName;
        }
    }
}