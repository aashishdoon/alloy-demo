﻿using System.Collections.Generic;
using System.Linq;
using Alloy.Demo.Models.Pages;
using EPiServer.Core;
using EPiServer.Validation;

namespace Alloy.Demo.Business.PageValidators
{
    public class EventPageValidator: IValidate<EventPage>
    {
        public IEnumerable<ValidationError> Validate(EventPage eventPage)
        {
            if (eventPage.EndDate <= eventPage.StartDate)
            {
                return new[]
                {
                    new ValidationError
                    {
                        ErrorMessage = "End Date must not be before the Start Date",
                        PropertyName = eventPage.GetPropertyName(page => page.EndDate),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    }
                };
            }

            return Enumerable.Empty<ValidationError>();

        }
    }
}