﻿using System;
using System.Collections.Generic;
using Alloy.Demo.Business.SelectionFactories;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Alloy.Demo.Business.EditorDescriptors
{
    public class EnumEditorDescriptor<TEnum> : EditorDescriptor
    {
        public override void ModifyMetadata(
            ExtendedMetadata metadata,
            IEnumerable<Attribute> attributes)
        {
            this.SelectionFactoryType = typeof(EnumSelectionFactory<TEnum>);

            this.ClientEditingClass = "epi-cms/contentediting/editors/SelectionEditor";

            base.ModifyMetadata(metadata, attributes);
        }
    }
}