﻿using System.Collections.Generic;
using Alloy.Demo.Models.Pages;
using EPiServer.Core;

namespace Alloy.Demo.Business
{
    public interface IContentLocator
    {
        IEnumerable<EventPage> GetEventPages(ContentReference parent);
    }
}