﻿using System.Web.Mvc;
using Alloy.Demo.Business;
using Alloy.Demo.Models.Pages;
using Alloy.Demo.Models.ViewModels;

namespace Alloy.Demo.Controllers
{
    public class EventListingPageController : PageControllerBase<EventListingPage>
    {
        private readonly IContentLocator _contentLocator;
        public EventListingPageController(IContentLocator contentLocator)
        {
            _contentLocator = contentLocator;
        }

        public ActionResult Index(EventListingPage currentPage)
        {
            var model = new EventListingViewModel(currentPage)
            {
                AllEvents = _contentLocator.GetEventPages(currentPage.ContentLink)
            };

            return View("~/Views/EventListPage/Index.cshtml", model);

        }

    }
}