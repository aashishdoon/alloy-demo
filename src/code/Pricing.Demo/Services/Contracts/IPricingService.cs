﻿using Pricing.Demo.Models;

namespace Pricing.Demo.Services.Contracts
{
    public interface IPricingService
    {
        //decimal GetPrice(Product product, User user);
        PriceResult GetPrice(Product product, User user);
    }
}