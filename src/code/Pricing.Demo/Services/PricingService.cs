﻿using Pricing.Demo.Models;
using Pricing.Demo.Helpers;
using Pricing.Demo.Utilities;
using Pricing.Demo.Services.Contracts;

namespace Pricing.Demo.Services
{
    public class PricingService : IPricingService
    {
        public PriceResult GetPrice(Product product, User user)
        {
            if (!user.IsLoggedIn)
            {
                return new PriceResult
                {
                    Price = 0,
                    ShowPrice = false,
                    Message = Constants.ResultMsg.UserNotLoggedIn
                };
            }

            var totalDiscount = user.OrgDiscount;

            if (product.OnlineDiscount)
            {
                // If products eligible for online discount
                totalDiscount += Constants.Discount.OnlineDiscount;
            }

            if (product.MaxDiscount != null && totalDiscount > product.MaxDiscount)
            {
                // Discount cannot be higher than Max discount
                totalDiscount = product.MaxDiscount.Value;
            }

            // Calculate customer price based on discount 
            var customerPrice = product.CataloguePrice.Discount(totalDiscount);

            return new PriceResult
            {
                Price = customerPrice,
                ShowPrice = true,
                Message = $"{Constants.ResultMsg.UserLoggedIn} {customerPrice}."
            };
        }
    }
}