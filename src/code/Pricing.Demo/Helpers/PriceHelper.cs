﻿namespace Pricing.Demo.Helpers
{
    public static class PriceHelper
    {
        public static decimal Discount(this decimal value, float discount)
        {
            return value - value * (decimal)discount / 100;
        }
    }
}