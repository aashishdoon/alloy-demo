﻿namespace Pricing.Demo.Models
{
    public class PriceResult
    {
        public decimal Price { get; set; }
        public bool ShowPrice { get; set; }
        public string Message { get; set; }
    }
}