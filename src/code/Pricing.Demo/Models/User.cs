﻿namespace Pricing.Demo.Models
{
    public class User
    {
        public bool IsLoggedIn { get; set; }
        public float OrgDiscount { get; set; }
    }
}