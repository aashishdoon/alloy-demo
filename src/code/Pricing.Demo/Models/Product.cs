﻿namespace Pricing.Demo.Models
{
    public class Product
    {
        public decimal CataloguePrice { get; set; }
        public float? MaxDiscount { get; set; }
        public bool OnlineDiscount { get; set; }  
    }
}