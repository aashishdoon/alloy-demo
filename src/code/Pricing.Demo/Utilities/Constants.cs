﻿namespace Pricing.Demo.Utilities
{
    public class Constants
    {
        public static class Discount
        {
            public const float OnlineDiscount = 1.5f;
        }

        public static class ResultMsg
        {
            public const string UserLoggedIn = "User is logged in. Customer price =";
            public const string UserNotLoggedIn = "User not logged in";
        }
    }
}